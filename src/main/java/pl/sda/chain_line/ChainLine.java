package pl.sda.chain_line;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class ChainLine extends JFrame implements ActionListener {


    private JLabel lLLP, lLLT, lDifference, lA, lB, lC, lD, lE, lListE, lLLPResult, lLLTResult, lDifferenceResult;
    private JTextField tA, tB, tC, tD, tE;
    private JComboBox cbListE;
    private JButton bCalculate;

    private ChainLine() {
        setTitle("Chain Line Calculator");
        setSize(800, 900);
        setLayout(null);

        JPanel obrazPanel = new ObrazPanel();
        obrazPanel.setBounds(350, 50, 600, 800);
        add(obrazPanel);

        lLLP = new JLabel("LLP");
        lLLP.setBounds(100, 40, 100, 20);
        lLLP.setForeground(Color.GREEN);
        lLLP.setBackground(Color.BLACK);
        lLLP.setOpaque(true);
        lLLP.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lLLP);

        lLLPResult = new JLabel("");
        lLLPResult.setBounds(210, 40, 100, 20);
        lLLPResult.setForeground(Color.GREEN);
        lLLPResult.setBackground(Color.BLACK);
        lLLPResult.setOpaque(true);
        lLLPResult.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lLLPResult);

        lLLT = new JLabel("LLT");
        lLLT.setBounds(100, 80, 100, 20);
        lLLT.setForeground(Color.GREEN);
        lLLT.setBackground(Color.BLACK);
        lLLT.setOpaque(true);
        lLLT.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lLLT);

        lLLTResult = new JLabel("");
        lLLTResult.setBounds(210, 80, 100, 20);
        lLLTResult.setForeground(Color.GREEN);
        lLLTResult.setBackground(Color.BLACK);
        lLLTResult.setOpaque(true);
        lLLTResult.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lLLTResult);


        lDifference = new JLabel("Difference");
        lDifference.setBounds(100, 120, 100, 20);
        lDifference.setForeground(Color.GREEN);
        lDifference.setBackground(Color.BLACK);
        lDifference.setOpaque(true);
        lDifference.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lDifference);

        lDifferenceResult = new JLabel("");
        lDifferenceResult.setBounds(210, 120, 100, 20);
        lDifferenceResult.setForeground(Color.GREEN);
        lDifferenceResult.setBackground(Color.BLACK);
        lDifferenceResult.setOpaque(true);
        lDifferenceResult.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lDifferenceResult);
        lDifferenceResult.setToolTipText("Difference should be below +/- 5mm");


        lA = new JLabel("A");
        lA.setBounds(100, 160, 100, 20);
        lA.setForeground(Color.GREEN);
        lA.setBackground(Color.BLACK);
        lA.setOpaque(true);
        lA.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lA);

        tA = new JTextField("0");
        tA.setBounds(210, 160, 100, 20);
        tA.setForeground(Color.GREEN);
        tA.setBackground(Color.BLACK);
        tA.setOpaque(true);
        tA.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(tA);
        tA.setToolTipText("Distance between middle sprocket of crankset and outer edge of seat tube");


        lB = new JLabel("B");
        lB.setBounds(100, 200, 100, 20);
        lB.setForeground(Color.GREEN);
        lB.setBackground(Color.BLACK);
        lB.setOpaque(true);
        lB.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lB);


        tB = new JTextField("0");
        tB.setBounds(210, 200, 100, 20);
        tB.setForeground(Color.GREEN);
        tB.setBackground(Color.BLACK);
        tB.setOpaque(true);
        tB.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(tB);
        tB.setToolTipText("Outer diameter of seat tube");

        lC = new JLabel("C");
        lC.setBounds(100, 240, 100, 20);
        lC.setForeground(Color.GREEN);
        lC.setBackground(Color.BLACK);
        lC.setOpaque(true);
        lC.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lC);

        tC = new JTextField("0");
        tC.setBounds(210, 240, 100, 20);
        tC.setForeground(Color.GREEN);
        tC.setBackground(Color.BLACK);
        tC.setOpaque(true);
        tC.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(tC);
        tC.setToolTipText("Use calliper to measure this size");

        lD = new JLabel("D");
        lD.setBounds(100, 280, 100, 20);
        lD.setForeground(Color.GREEN);
        lD.setBackground(Color.BLACK);
        lD.setOpaque(true);
        lD.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lD);

        tD = new JTextField("0");
        tD.setBounds(210, 280, 100, 20);
        tD.setForeground(Color.GREEN);
        tD.setBackground(Color.BLACK);
        tD.setOpaque(true);
        tD.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(tD);
        tD.setToolTipText("Use feeler gauge to measure this size");

        lE = new JLabel("E");
        lE.setBounds(100, 320, 100, 20);
        lE.setForeground(Color.GREEN);
        lE.setBackground(Color.BLACK);
        lE.setOpaque(true);
        lE.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lE);

        tE = new JTextField("0");
        tE.setBounds(210, 320, 100, 20);
        tE.setForeground(Color.GREEN);
        tE.setBackground(Color.BLACK);
        tE.setOpaque(true);
        tE.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(tE);

        lListE = new JLabel("E list of cassettes/freewheels ");
        lListE.setBounds(100, 360, 240, 20);
        lListE.setForeground(Color.GREEN);
        lListE.setBackground(Color.BLACK);
        lListE.setOpaque(true);
        lListE.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(lListE);

        cbListE = new JComboBox();
        cbListE.setBounds(100, 400, 240, 20);
        cbListE.setForeground(Color.GREEN);
        cbListE.setBackground(Color.BLACK);
        cbListE.setOpaque(true);
        cbListE.setFont(new Font("SansSerif", Font.BOLD, 14));
        cbListE.addItem("Campagnolo 10speed: 38,8mm");
        cbListE.addItem("Shimano 10speed: 37,2mm");
        cbListE.addItem("Campagnolo 9speed: 38,2mm");
        cbListE.addItem("Shimano 9speed: 36,3mm");
        cbListE.addItem("SRAM 9speed: 36,5mm");
        cbListE.addItem("Campagnolo 8speed: 36,9mm");
        cbListE.addItem("Shimano 8speed: 35,4mm");
        cbListE.addItem("SRAM 8speed: 35,4mm");
        cbListE.addItem("Shimano 7speed: 31,9mm");
        cbListE.addItem("SRAM FR 8speed: 36,8mm ");
        cbListE.addItem("SRAM FR 7speed: 31,8mm");
        add(cbListE);
        cbListE.addActionListener(this);


        bCalculate = new JButton("Calculate LLP LLT Difference");
        bCalculate.setBounds(100, 440, 240, 20);
        bCalculate.setForeground(Color.GREEN);
        bCalculate.setBackground(Color.BLACK);
        bCalculate.setOpaque(true);
        bCalculate.setFont(new Font("SansSerif", Font.BOLD, 14));
        add(bCalculate);
        bCalculate.addActionListener(this);
    }


    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();
        if (source == cbListE) {
            String cassetteWidth = cbListE.getSelectedItem().toString();

            if (cassetteWidth.equals("Campagnolo 10speed: 38,8mm")) {
                tE.setText("38.8");
            } else if (cassetteWidth.equals("Shimano 10speed: 37,2mm")) {
                tE.setText("37.2");
            } else if (cassetteWidth.equals("Campagnolo 9speed: 38,2mm")) {
                tE.setText("38.2");
            } else if (cassetteWidth.equals("Shimano 9speed: 36,3mm")) {
                tE.setText("36.3");
            } else if (cassetteWidth.equals("SRAM 9speed: 36,5mm")) {
                tE.setText("36.5");
            }else if (cassetteWidth.equals("Campagnolo 8speed: 36,9mm")) {
                tE.setText("36.9");
            }else if (cassetteWidth.equals("Shimano 8speed: 35,4mm")) {
                tE.setText("35.4");
            }else if (cassetteWidth.equals("SRAM 8speed: 35,4mm")) {
                tE.setText("35.4");
            }else if (cassetteWidth.equals("Shimano 7speed: 31,9mm")) {
                tE.setText("31.9");
            }else if (cassetteWidth.equals("SRAM FR 8speed: 36,8mm ")) {
                tE.setText("36.8");
            }else if (cassetteWidth.equals("SRAM FR 7speed: 31,8mm")) {
                tE.setText("31.8");
            }


        }else if(source==bCalculate){
            try{
            double a=Double.parseDouble(tA.getText());
            double b=Double.parseDouble(tB.getText());
            double c=Double.parseDouble(tC.getText());
            double d=Double.parseDouble(tD.getText());
            double e=Double.parseDouble(tE.getText());

            if(a>0.0&&a<200.0&&b>0.0&&b<200.0&&c>0.0&&c<200.0&&d>0.0&&d<200.0&&e>0.0&&e<200.0) {
                double llp = a + 0.5 * b;
                double llt = 0.5 * c - (d + 0.5 * e);
                double difference = llp - llt;
                if (difference>5.0||difference<-5.0){
                    lDifferenceResult.setForeground(Color.RED);
                }
                lLLPResult.setText(String.format("%.2f", llp));
                lLLTResult.setText(String.format("%.2f", llt));
                lDifferenceResult.setText(String.format("%.2f", difference));
            }
            }catch (NumberFormatException e){}
        }

    }
        public static void main (String[]args){
            ChainLine chainLine = new ChainLine();
            chainLine.setDefaultCloseOperation(EXIT_ON_CLOSE);
            chainLine.setVisible(true);
        }


}
